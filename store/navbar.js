export const state = () => ({
  isCollapse: true,
  sections: [
    {
      label: 'Nosotros',
      id: 'SectionUs'
    },
    {
      label: '¡Hazlo tu mismo!',
      id: 'SectionDoYourself'
    },
    {
      label: 'Servicios',
      id: 'SectionServices'
    },
    {
      label: 'Portafolio',
      id: 'SectionBriefcase'
    },
    {
      label: 'Contáctanos',
      id: 'SectionContactUs'
    }
  ]
})

export const mutations = {
  SET_COLLAPSE(state, isCollapse) {
    state.isCollapse = isCollapse
  }
}

export const actions = {
  setCollapse({ commit }, isCollapse) {
    commit('SET_COLLAPSE', isCollapse)
  }
}

export const getters = {
  getCollapse(state) {
    return state.isCollapse
  }
}
