export const state = () => ({
  numberDesign: '',
  claiming: ''
})

export const mutations = {
  SET_NUMBER_DESIGN(state, numberDesign) {
    state.numberDesign = numberDesign
  },
  SET_CLAIMING(state, claiming) {
    state.claiming = claiming
  }
}

export const actions = {
  setNumberDesign({ commit }, numberDesign) {
    commit('SET_NUMBER_DESIGN', numberDesign)
  }
}

export const getters = {
  getNumberDesign(state) {
    return state.numberDesign
  }
}
