export const state = () => ({
  fromForm: false,
  fromDemo: false,
  developing: true,
  serviceContact: '',
  infoGeneral: {
    logo: '',
    email: '',
    facebook: '',
    instagram: '',
    linkedin: '',
    phone: '',
    whatsapp: ''
  },
  loadedInfoGeneral: false,
  canGoBack: false,
  previsualizing: false,
  step: 0
})

export const mutations = {
  SET_FROM_FORM(state, fromForm) {
    state.fromForm = fromForm
  },
  SET_FROM_DEMO(state, fromDemo) {
    state.fromDemo = fromDemo
  },
  SET_SERVICE_CONTACT(state, title) {
    state.serviceContact = title
  },
  SET_INFO_GENERAL(state, infoGeneral) {
    state.infoGeneral = infoGeneral
    state.loadedInfoGeneral = true
  },
  SET_PREVISUALIZING(state, value) {
    state.previsualizing = value
  },
  SET_CAN_GO_BACK(state, value) {
    state.canGoBack = value
  },
  SET_STEP(state, value) {
    state.step = value
  },
}

export const actions = {
  setFromForm({ commit }, fromForm) {
    commit('SET_FROM_FORM', fromForm)
  },
  setFromDemo({ commit }, fromDemo) {
    commit('SET_FROM_DEMO', fromDemo)
  }
}

export const getters = {
  getFromForm(state) {
    return state.fromForm
  }
}
