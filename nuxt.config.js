require('dotenv').config()

const baseURL = process.env.API_URL

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'PASA TU CV',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'shortcut icon', type: 'image/png', href: '/logo.png' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/element-ui.css',
    '~/assets/scss.scss',
    '~/assets/font.scss',
    'swiper/css/swiper.min.css',
    'element-ui/lib/theme-chalk/index.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui',
    { src: './plugins/vue-awesome-swiper.js', ssr: false },
    { src: '~/plugins/vee-validate', ssr: false },
    { src: '~/plugins/tinymce.js', ssr: false },
    { src: '~/plugins/custom-functions', ssr: false }
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    // '@nuxtjs/auth',
    '@nuxtjs/pwa'
  ],
  env: {
    // GMAPS_KEY: process.env.GMAPS_KEY
  },
  axios: {
    baseURL: baseURL,
    https: false
  },
  // auth: {
  //   token: {
  //     prefix: '_token.'
  //   },
  //   redirect: {
  //     login: '/',
  //     logout: '/',
  //     home: '/dashboard/'
  //   },
  //   strategies: {
  //     local: {
  //       endpoints: {
  //         login: { url: '/auth/login', method: 'post', propertyName: 'token' },
  //         logout: false,
  //         user: { url: '/user/me', method: 'get', propertyName: false }
  //       }
  //     }
  //   }
  // },
  server: {
    port: 3002, // default: 3000
    host: "0.0.0.0"
  },
  build: {
    transpile: [/^element-ui/],
    /** You can extend webpack config here */
    extend (config, ctx) {
    }
  }
}
