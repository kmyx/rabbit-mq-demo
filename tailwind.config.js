module.exports = {
  purge: false,
  // important: true,
  theme: {
    fontFamily: {
      'font-Gotham': ['Gotham', 'sans-serif']
    },
    extend: {
      colors: {
        'main-blue': '#415595',
        'main-blue-hover': '#7b8cc3',
        'main-rose': '#DFB1D3',
        'main-skyblue': '#B3DAE1',
        'main-green': '#59BAB2',
        'main-gray': '#757575',
        'main-lightgray': '#C4C4C4',
        'main-lightgreen': '#5FE23E',
        'main-lightestgray': '#E7E7E7',
        'd1-brown': '#C0AA98',
        'd1-lightbrown': '#F4F0EF',
        'd1-blue': '#365A6C',
        'd2-lightgray': '#e2e3e7',
        'd2-blue': '#51628e',
        'd2-green': '#365a6c',
        'd2-gray': '#6c6c6c',
        'd3-brown': '#b09178',
        'd3-turquoise': '#446677',
        'd3-lightturquoise': '#b1c0c7',
        'd3-green': '#9abec3',
        'd3-lightbrown': '#dfd3c9',
        'd4-blue': '#617190',
        'd4-gree': '#abdacd',
        'd5-rose': '#dba19f',
        'd5-darkgray': '#525355',
        'd5-lightgray': '#f2f2f2',
        'd5-green': '#4c696d',
        'd7-green': '#365a6c',
        'd7-blue': '#ebedfc',
        'd7-lightgreen': '#82afb5',
        'd7-gray': '#9b9796',
        'd8-gray': '#969799',
        'd9-green': '#365a6c',
        'd9-gray': '#545068',
        'd9-lightgray': '#cbced5',
        'd10-black': '#2b2b2b',
        'd10-gray': '#3f3f3f',
      },
      height: {
        '1/2': '50%'
      }
    }
  },
  variants: {
    opacity: ['responsive', 'hover']
  }
}