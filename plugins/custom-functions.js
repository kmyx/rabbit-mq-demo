import Vue from 'vue'

Vue.prototype.$wait = n => new Promise(resolve => setTimeout(resolve, n))

Vue.prototype.$goTo = async (slug) => {
  try {
    // router.push('/')
    // await Vue.prototype.$wait(200)
    const el = document.getElementById(slug)
    if (el) {
      if (window.innerWidth > 640) {
        window.scroll({ top: el.offsetTop - 90, behavior: 'smooth' })
      } else {
        window.scroll({ top: el.offsetTop - 74, behavior: 'smooth' })
      }
    }
  } catch (error) {
    // console.log(error)
  }
}

Vue.prototype.$getApiImage = () => {
  return process.env.API_URL + '/storage/'
}

Vue.prototype.$generateUid = () => {
  // desired length of Id
  var idStrLen = 32;
  // always start with a letter -- base 36 makes for a nice shortcut
  var idStr = (Math.floor((Math.random() * 25)) + 10).toString(36) + "_";
  // add a timestamp in milliseconds (base 36 again) as the base
  idStr += (new Date()).getTime().toString(36) + "_";
  // similar to above, complete the Id using random, alphanumeric characters
  do {
      idStr += (Math.floor((Math.random() * 35))).toString(36);
  } while (idStr.length < idStrLen);
  return (idStr);
}

Vue.prototype.$slugify = str => {
  str = str.replace(/^\s+|\s+$/g, '')
  str = str.toLowerCase()
  // remove accents, swap ñ for n, etc
  const from = 'àáäâèéëêìíïîòóöôùúüûñç·/_,:;'
  const to = 'aaaaeeeeiiiioooouuuunc------'
  for (let i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }
  str = str
    .replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes
  return str
}

Vue.prototype.$getBase64 = file => {
  return new Promise((resolve, reject) => {
    const reader = new window.FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })
}

Vue.prototype.$dataURLtoFile = (dataurl, filename) => {
  let arr = dataurl.split(',')
  let mime = arr[0].match(/:(.*?);/)[1]
  let bstr = atob(arr[1])
  let n = bstr.length
  let u8arr = new Uint8Array(n)
  while(n--){
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new File([u8arr], filename, {type:mime})
}

Vue.prototype.$getMonthInSpanish = date => {
  try {
    if (date) {
      let arr = date.split('/')
      let monthString = ''
      let months = {
        '01': 'Enero',
        '02': 'Febrero',
        '03': 'Marzo',
        '04': 'Abril',
        '05': 'Mayo',
        '06': 'Junio',
        '07': 'Julio',
        '08': 'Agosto',
        '09': 'Septiembre',
        '10': 'Octubre',
        '11': 'Noviembre',
        '12': 'Diciembre',
      }
      return `${monthString} ${months[arr[1]]}`
    } else {
      return ''
    }
  } catch (e) {
    return ''
  }
}